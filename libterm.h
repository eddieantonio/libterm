/**
 * @file libterm.h
 * @brief Header file for libterm.c
 * @author Eddie Antonio Santos <easantos@ualberta.ca>
 *
 * @see libterm.c
 * @see libterm_nif.c
 */

int edit_distance(char*,char*);
double percent_difference(char*,char*);

