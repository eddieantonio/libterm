-module(libterm).
-author("Eddie Antonio Santos <easantos@ualberta.ca>").
-export([edit_distance/2]).
-export([percent_difference/2]).
-on_load(init/0).

% Note: Mostly adapted from http://www.erlang.org/doc/tutorial/nif.html

init() ->
    ok = erlang:load_nif("./libterm_nif", 0).

edit_distance(_Str1, _Str2) ->
    exit(nif_library_not_loaded).

percent_difference(_Str1, _Str2) ->
    exit(nif_library_not_loaded).

