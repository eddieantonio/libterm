/**
 * @file libterm.c
 * @brief Library for term analysis.
 * @author Eddie Antonio Santos <easantos@ualberta.ca>
 *
 */

#include <stdlib.h>
#include <string.h>
#include "libterm.h"

/** Type of character. typedef'd for easy changing later. */
typedef char letter;
/** Thing that tests the length of our custom character type. */
#define letterlen(x)   strlen(x)

/* Make things a bit tidier. */
struct comparison_info {
    letter *    str1;
    int         str1len;

    letter *    str2;
    int         str2len;

    int *       d;      // *The* array. (Say it in Jamaican.)
};

/* Utility. */
static int * prepare_array(int, int);
static void unprepare_array(int *);

/* Ease of reading. */
static int last_letter_eq(struct comparison_info*, int, int);
static int transpostion_exists(struct comparison_info*, int, int);

/* Things that should exist but don't. */
static int min(int,int);
static int trimin(int,int,int);

/** 
 * Damerau-Levenshtein edit distance algorithm. Find out how many "steps" it
 * takes to transform one string into the other.
 *
 * @param   str1,str2
 *          Two strings to compare. For example, the second string could be
 *          the term you'd like to match against.
 *
 * @return  Returns the smallest edit distance between two strings.
 * @retval  If some bizzare error occurs, -1 will be returned.
 * 
 * Thanks: http://www.devnetwork.net/viewtopic.php?f=50&t=89094
 * Refer to the Wikipedia article on this algorithm.
 */
int edit_distance(letter *str1, letter *str2) {

    /* Put things in a handy struct, to keep things tidy. */
    struct comparison_info info; 
    info.str1 = str1;
    info.str2 = str2;

    /* Get length of both strings. */
    info.str1len = letterlen(str1);
    info.str2len = letterlen(str2);

    int m, n;
    m = info.str1len;
    n = info.str2len;

    /* If either of the strings are empty, just return the length of the
     * other string. */
    if (m < 1) {
        return n;
    }
    if (n < 1) {
        return m;
    }

    /* d is our Dynamicprogramming Distance Darray. */
    int *d = prepare_array(m, n);
    info.d = d;

    if (d == NULL) {
        // Aw man! We gotsta abort this; t'ain't no memory left (???).
        return -1;
    }

    int i, j, cost;

    /* This minor abuse of macros make the code look cleaner.
     * #undef this quickly, though! */
#   define d(i,j)    d[(i) + (m + 1) * (j)]

    /* Matrix is size of m+1 x n+1; ignore d[0,0]. */
    for (i = 1; i <= m; i++) {
        for (j = 1; j <= n; j++) {

            cost = last_letter_eq(&info, i, j);

            d(i, j) = trimin(
                d(i - 1, j) + 1,           // Deletion
                d(i, j - 1) + 1,           // Insertion
                d(i - 1, j - 1) + cost     // Substitution
            );

            if (transpostion_exists(&info, i, j)) {
                d(i, j) = min(
                    d(i, j),
                    d(i - 2, j - 2) + cost
                );
            }
        }
    }


    /* The last entry in the array is the answer. */
    int distance = d(m, n);
#   undef d

    unprepare_array(d);
    d = NULL;

    return distance;
}

/**
 * How different is string one from string two, given as a percentage
 * (i.e., a floating-point value in [0, 1]).
 *
 * @param   base    The 'base' string. Comparison will be made relative
 *                  to this string.
 * @param   weirdo  String to compare the base against.
 *
 * @return  The percentage of difference, given as a double.
 *          `0` means the strings are exactly the same; 
 *          `1` means they're very, very different. Identically-lengthen
 *          strings with not even one character being able to be
 *          swapped, deleted, inserted or substituted are said to be
 *          completely different.
 * @retval  Will return -1.0 if a codeshark eats up important bytes.
 */
double percent_difference(letter *base, letter *weirdo) {

    int baseLength = letterlen(base);
    int distance = edit_distance(base, weirdo);

    if (distance < 0) {
        /* Handle when an error occurs. */
        return -1.0;
    } if (distance == 0) {
        /* Strings are *exactly* the same. Return 0% difference. */
        return 0.0;
    } else if (distance >= baseLength) {
        /* We consider these strings to be as different as can be. */
        return 1.0;
    }

    /* Davood said to make it this. Dunno why lol. */
    double similarity;
    similarity = (double) distance / (double) baseLength;

    return similarity;

}

static int * prepare_array(int m, int n) {
    /* D-L requires an extra row and column on the top and left,
     * respectively, to be "gutters". Believe me, it's magical. */
    int width, height, size;
    width = m + 1;
    height = n + 1;
    size = width * height;

    int *d;
    d = (int *) calloc(size, sizeof(int));

    if (d == NULL) {
        /* Return if we've somehow been unable to allocate memory. */
        return NULL;
    } 

    /* Prepare the gutters with initial values. */
    int i;
    for(i = 1; i < width; i++) {
        d[i] = i; 
    }
    for(i = 1; i < height; i++){
        d[i * width] = i; 
    }

    return d;
}

static void unprepare_array(int *d) {
    free((void *) d);
}


static int last_letter_eq(struct comparison_info *info, int i, int j) {
    letter * str1 = info->str1;
    letter * str2 = info->str2;

    if (str1[i - 1] == str2[j - 1]) {
        return 0;
    } 

    return 1;

}

static int transpostion_exists(struct comparison_info *info, int i, int j) {
    letter * str1 = info->str1;
    letter * str2 = info->str2;

    return  i > 1 && j > 1 // String is long enough to even *have* a transposition.
            && 
                (str1[i - 1] == str2[j - 2])  // Can the last two
            && 
                (str1[i - 2] == str2[j - 1]); // letters be swapped?

}

static int min(int a, int b) {
    return a < b ? a : b;
}

static int trimin(int a, int b, int c) {
    return min(min(a, b), c);
}

