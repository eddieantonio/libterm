/**
 * @file termtest.c
 * @brief Test for libterm.
 * @author Eddie Antonio Santos <easantos@ualberta.ca>
 *
 */

#include <stdio.h>
#include "libterm.h"

#define START_TESTING() \
    int errors = 0, count = 1;  

#define TEST(expr, expected)    \
    printf("Test %d ", count);  \
    if ((expr) == (expected)) { \
       printf("[32msucceeded[0m");  \
    } else {        \
        errors++;   \
        printf("[31mfailed[0m");    \
    }   \
    printf("\nTest was: " #expr       \
            "; Expected: " #expected    \
            "\n"); \
    count++

int main() {

    START_TESTING();

    // Both strings empty
    TEST(edit_distance("", ""),             0);
    // Empty string 1
    TEST(edit_distance("", "foo"),          3);
    // Empty string 2
    TEST(edit_distance("foo", ""),          3);
    // Equality
    TEST(edit_distance("foo", "foo"),       0);
    // One letter typo
    TEST(edit_distance("fool", "foul"),     1);
    // Transposition
    TEST(edit_distance("this", "tihs"),     1);
    // Common English typo (homophone).
    TEST(edit_distance("there", "their"),   2);

    TEST(edit_distance("foo", "bar"),       3);

    // International -- currently fails at UTF-8
    TEST(edit_distance("Der Erlkonig",
                       "Der Erlkönig"),     1);

    // Different strings of same length
    TEST(edit_distance("Eddie", "Sasha"),   5);
    TEST(edit_distance("qwertyuiop", "asdfghjkl;"),   10);

    // Test an completely different string.
    TEST(percent_difference("qwertyuiop", "asdfghjkl;"),            1.00);
    // Test two very long identical strings.
    TEST(percent_difference("Supercalifragilisticexpealidocious",
                            "Supercalifragilisticexpealidocious"),  0.00);
    // Real misspellings, caught from the wild.
    TEST(percent_difference("Fredericton", "Fredricton"),       1.0/11.0);
    TEST(percent_difference("Chrysler", "Chrystler"),            1.0/8.0);

    return errors;
}

