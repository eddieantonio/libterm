/**
 * @file libterm_nif.c
 * @brief Erlang NIF wrapper for term analysis.
 * @author Eddie Antonio Santos <easantos@ualberta.ca>
 *          (C/Erlang NIF implementation)
 * @author Stephen Kalen Romansky <romansky@ualberta.ca>
 *          (Original PHP implementation)
 *
 */

#include <stdlib.h>
#include <stdbool.h>
#include "erl_nif.h"

#include "libterm.h"

/* Make strings out of contiguous Erlang terms from source into the
 * (predefined) array of strings called `destination`. */
static bool getStrings(ErlNifEnv *env, int howMany,
       const ERL_NIF_TERM source[], char* destination[])
{
    int i;
    bool gotLength;
    unsigned int lengths[howMany];
    
    /* Verify lengths before mallocing and finding a bad argument (which
     * would involve having to free all that unused heap memory!) */
    for (i = 0; i < howMany; i++) {
        gotLength = enif_get_list_length(env, source[i], &(lengths[i]));
        if (!gotLength) {
            return false;
        }
    }

    /* Now that all arguments are known to be strings, allocate and
     * set them. */
    for (i = 0; i < howMany; i++) {
        /* We need room for that '\0'! */
        lengths[i]++;
        
        char *newString = (char *) malloc(sizeof(char) * (lengths[i]));
        enif_get_string(env, source[i], newString, lengths[i],
                ERL_NIF_LATIN1);
        destination[i] = newString;

    }

    return true;

}
        

static ERL_NIF_TERM edit_distance_nif(
        ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{

    char *strings[2] = {NULL, NULL};

    /* Try to get both strings; Fail if they're empty or not strings. */
    if (!getStrings(env, 2, argv, strings)) {
        return enif_make_badarg(env);
    }

    int distance = edit_distance(strings[0], strings[1]);
    free(strings[0]); free(strings[1]);

    if (distance < 0) {
        /* `edit_distance` throws an error by giving a negative
         * distance; in Erlang, we give back an 'error' atom. */
        return enif_make_atom(env, "error");
    }

    return enif_make_int(env, distance);
}

static ERL_NIF_TERM percent_difference_nif(
        ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{

    char *strings[2];

    /* Try to get both strings; Fail if they're empty or not strings. */
    if (!getStrings(env, 2, argv, strings)) {
        return enif_make_badarg(env);
    }

    double difference = percent_difference(strings[0], strings[1]);
    free(strings[0]); free(strings[1]);

    if (difference < 0.0) {
        /* An error occured! Report it. */
        return enif_make_atom(env, "error");
    }

    return enif_make_double(env, difference);
}

#define DECLARE_ENIF_FUNC(funcname, arity) \
    {#funcname, arity, funcname ## _nif }

static ErlNifFunc nif_funcs[] = {
    DECLARE_ENIF_FUNC(edit_distance, 2),
    DECLARE_ENIF_FUNC(percent_difference, 2),
};

ERL_NIF_INIT(libterm, nif_funcs, NULL, NULL, NULL, NULL)

