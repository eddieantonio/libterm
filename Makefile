CC = gcc
CFLAGS = -Wall -pedantic -std=c99

INC=/usr/lib/erlang/usr/include/
CPPFLAGS += -I$(INC)

UNAME := $(shell uname)

# Silly OS X!
ifeq ($(UNAME), Darwin)
LDFLAGS_SHARED = -undefined dynamic_lookup -dynamiclib
CFLAGS += -arch i386 -arch x86_64
else
LDFLAGS_SHARED = -fPIC -shared
endif

ERL = erlc
ERLFLAGS = -Wall

.SUFFIXES: .erl .beam .c .so

.erl.beam:
	$(ERL) $(ERLFLAGS) $<

.c.so:
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS_SHARED) -o $@ $^

MODS = libterm

all: compile

compile: ${MODS:%=%.beam} ${MODS:%=%_nif.so}

clean:
	-rm -rf *.beam *.o *.so erl_crash.dump term_test

test: term_test
	./$<

# Specific rules.
libterm_nif.so: libterm.o libterm_nif.c

term_test: libterm.o term_test.o

.PHONY: all compile clean test
